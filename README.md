# TDT4250 
## Members:
Mathias Myrold, Håvard Tysland and Ole Løkken

We have modeled Netflix. In netflix the user can add movies and series to "My List" (favorites). We have modeled this list and include one movie(Forrest Gump) and one series (Peay blinders). In addition we have modeled the pages for the movie/series.

## Constrains 
We made a constrain for the age of the actor.

## Datatypes
Own datatype for the birthdate for an actor and a datatype MediaFile for the video file in a movie or series.

## Interface, Abstract class and Intereface
Made an interfcace for playing and pausing a video.
An Abstract class for a video, where movie and series extends the abstract class.
Also made Classes for all relevant points.

## Refrences 
Made bouth containments, opposites and ordinary


## Links and Screenshots:
[https://www.netflix.com/browse?jbv=80002479](url)

<img height="300px" width="500px" src="./Pictures/PeakyBlindersPage.png"/>


[https://www.netflix.com/profiles/manage](url)

<img height="300px" width="500px" src="./Pictures/Profiles.png"/>

[https://www.netflix.com/browse/my-list](url)

<img height="300px" width="500px" src="./Pictures/Favorites.png"/>


## Our model
<img  src="./Pictures/EcoreModel.png"/>


## Our instance
<img  src="./Pictures/instance.png"/>
