/**
 */
package Netflix;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Series</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link Netflix.Series#getSeasons <em>Seasons</em>}</li>
 * </ul>
 *
 * @see Netflix.NetflixPackage#getSeries()
 * @model
 * @generated
 */
public interface Series extends MediaContent {
	/**
	 * Returns the value of the '<em><b>Seasons</b></em>' containment reference list.
	 * The list contents are of type {@link Netflix.Season}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Seasons</em>' containment reference list.
	 * @see Netflix.NetflixPackage#getSeries_Seasons()
	 * @model containment="true" required="true"
	 * @generated
	 */
	EList<Season> getSeasons();

} // Series
