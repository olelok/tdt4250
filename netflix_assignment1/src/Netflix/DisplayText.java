/**
 */
package Netflix;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Display Text</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link Netflix.DisplayText#getText <em>Text</em>}</li>
 *   <li>{@link Netflix.DisplayText#getStartTime <em>Start Time</em>}</li>
 *   <li>{@link Netflix.DisplayText#getEndTime <em>End Time</em>}</li>
 * </ul>
 *
 * @see Netflix.NetflixPackage#getDisplayText()
 * @model
 * @generated
 */
public interface DisplayText extends EObject {
	/**
	 * Returns the value of the '<em><b>Text</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Text</em>' attribute.
	 * @see #setText(String)
	 * @see Netflix.NetflixPackage#getDisplayText_Text()
	 * @model
	 * @generated
	 */
	String getText();

	/**
	 * Sets the value of the '{@link Netflix.DisplayText#getText <em>Text</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Text</em>' attribute.
	 * @see #getText()
	 * @generated
	 */
	void setText(String value);

	/**
	 * Returns the value of the '<em><b>Start Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Start Time</em>' attribute.
	 * @see #setStartTime(int)
	 * @see Netflix.NetflixPackage#getDisplayText_StartTime()
	 * @model required="true"
	 * @generated
	 */
	int getStartTime();

	/**
	 * Sets the value of the '{@link Netflix.DisplayText#getStartTime <em>Start Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Start Time</em>' attribute.
	 * @see #getStartTime()
	 * @generated
	 */
	void setStartTime(int value);

	/**
	 * Returns the value of the '<em><b>End Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>End Time</em>' attribute.
	 * @see #setEndTime(int)
	 * @see Netflix.NetflixPackage#getDisplayText_EndTime()
	 * @model required="true"
	 * @generated
	 */
	int getEndTime();

	/**
	 * Sets the value of the '{@link Netflix.DisplayText#getEndTime <em>End Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>End Time</em>' attribute.
	 * @see #getEndTime()
	 * @generated
	 */
	void setEndTime(int value);

} // DisplayText
