/**
 */
package Netflix;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EOperation;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Playable</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see Netflix.NetflixPackage#getPlayable()
 * @model interface="true" abstract="true"
 * @generated
 */
public interface Playable extends EObject {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	EOperation play();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	EOperation pause();

} // Playable
