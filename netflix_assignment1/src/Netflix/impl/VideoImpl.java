/**
 */
package Netflix.impl;

import Netflix.NetflixPackage;
import Netflix.Subtitle;
import Netflix.Video;

import java.io.File;
import java.lang.reflect.InvocationTargetException;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Video</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link Netflix.impl.VideoImpl#getLength <em>Length</em>}</li>
 *   <li>{@link Netflix.impl.VideoImpl#getMediaFile <em>Media File</em>}</li>
 *   <li>{@link Netflix.impl.VideoImpl#getSubtitles <em>Subtitles</em>}</li>
 * </ul>
 *
 * @generated
 */
public class VideoImpl extends MinimalEObjectImpl.Container implements Video {
	/**
	 * The default value of the '{@link #getLength() <em>Length</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLength()
	 * @generated
	 * @ordered
	 */
	protected static final int LENGTH_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getLength() <em>Length</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLength()
	 * @generated
	 * @ordered
	 */
	protected int length = LENGTH_EDEFAULT;

	/**
	 * The default value of the '{@link #getMediaFile() <em>Media File</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMediaFile()
	 * @generated
	 * @ordered
	 */
	protected static final File MEDIA_FILE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getMediaFile() <em>Media File</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMediaFile()
	 * @generated
	 * @ordered
	 */
	protected File mediaFile = MEDIA_FILE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getSubtitles() <em>Subtitles</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSubtitles()
	 * @generated
	 * @ordered
	 */
	protected EList<Subtitle> subtitles;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected VideoImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return NetflixPackage.Literals.VIDEO;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getLength() {
		return length;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLength(int newLength) {
		int oldLength = length;
		length = newLength;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, NetflixPackage.VIDEO__LENGTH, oldLength, length));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public File getMediaFile() {
		return mediaFile;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMediaFile(File newMediaFile) {
		File oldMediaFile = mediaFile;
		mediaFile = newMediaFile;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, NetflixPackage.VIDEO__MEDIA_FILE, oldMediaFile, mediaFile));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Subtitle> getSubtitles() {
		if (subtitles == null) {
			subtitles = new EObjectContainmentEList<Subtitle>(Subtitle.class, this, NetflixPackage.VIDEO__SUBTITLES);
		}
		return subtitles;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation play() {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation pause() {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case NetflixPackage.VIDEO__SUBTITLES:
				return ((InternalEList<?>)getSubtitles()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case NetflixPackage.VIDEO__LENGTH:
				return getLength();
			case NetflixPackage.VIDEO__MEDIA_FILE:
				return getMediaFile();
			case NetflixPackage.VIDEO__SUBTITLES:
				return getSubtitles();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case NetflixPackage.VIDEO__LENGTH:
				setLength((Integer)newValue);
				return;
			case NetflixPackage.VIDEO__MEDIA_FILE:
				setMediaFile((File)newValue);
				return;
			case NetflixPackage.VIDEO__SUBTITLES:
				getSubtitles().clear();
				getSubtitles().addAll((Collection<? extends Subtitle>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case NetflixPackage.VIDEO__LENGTH:
				setLength(LENGTH_EDEFAULT);
				return;
			case NetflixPackage.VIDEO__MEDIA_FILE:
				setMediaFile(MEDIA_FILE_EDEFAULT);
				return;
			case NetflixPackage.VIDEO__SUBTITLES:
				getSubtitles().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case NetflixPackage.VIDEO__LENGTH:
				return length != LENGTH_EDEFAULT;
			case NetflixPackage.VIDEO__MEDIA_FILE:
				return MEDIA_FILE_EDEFAULT == null ? mediaFile != null : !MEDIA_FILE_EDEFAULT.equals(mediaFile);
			case NetflixPackage.VIDEO__SUBTITLES:
				return subtitles != null && !subtitles.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case NetflixPackage.VIDEO___PLAY:
				return play();
			case NetflixPackage.VIDEO___PAUSE:
				return pause();
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (length: ");
		result.append(length);
		result.append(", mediaFile: ");
		result.append(mediaFile);
		result.append(')');
		return result.toString();
	}

} //VideoImpl
