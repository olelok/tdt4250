/**
 */
package Netflix;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Profile</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link Netflix.Profile#getName <em>Name</em>}</li>
 *   <li>{@link Netflix.Profile#getFavorites <em>Favorites</em>}</li>
 * </ul>
 *
 * @see Netflix.NetflixPackage#getProfile()
 * @model
 * @generated
 */
public interface Profile extends EObject {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see Netflix.NetflixPackage#getProfile_Name()
	 * @model
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link Netflix.Profile#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Favorites</b></em>' containment reference list.
	 * The list contents are of type {@link Netflix.MediaContent}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Favorites</em>' containment reference list.
	 * @see Netflix.NetflixPackage#getProfile_Favorites()
	 * @model containment="true"
	 * @generated
	 */
	EList<MediaContent> getFavorites();

} // Profile
