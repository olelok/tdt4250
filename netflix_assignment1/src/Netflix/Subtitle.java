/**
 */
package Netflix;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Subtitle</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link Netflix.Subtitle#getLanguage <em>Language</em>}</li>
 *   <li>{@link Netflix.Subtitle#getDisplayTexts <em>Display Texts</em>}</li>
 * </ul>
 *
 * @see Netflix.NetflixPackage#getSubtitle()
 * @model
 * @generated
 */
public interface Subtitle extends EObject {
	/**
	 * Returns the value of the '<em><b>Language</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Language</em>' attribute.
	 * @see #setLanguage(String)
	 * @see Netflix.NetflixPackage#getSubtitle_Language()
	 * @model required="true"
	 * @generated
	 */
	String getLanguage();

	/**
	 * Sets the value of the '{@link Netflix.Subtitle#getLanguage <em>Language</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Language</em>' attribute.
	 * @see #getLanguage()
	 * @generated
	 */
	void setLanguage(String value);

	/**
	 * Returns the value of the '<em><b>Display Texts</b></em>' containment reference list.
	 * The list contents are of type {@link Netflix.DisplayText}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Display Texts</em>' containment reference list.
	 * @see Netflix.NetflixPackage#getSubtitle_DisplayTexts()
	 * @model containment="true"
	 * @generated
	 */
	EList<DisplayText> getDisplayTexts();

} // Subtitle
