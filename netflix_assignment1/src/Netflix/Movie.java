/**
 */
package Netflix;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Movie</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link Netflix.Movie#getVideo <em>Video</em>}</li>
 * </ul>
 *
 * @see Netflix.NetflixPackage#getMovie()
 * @model
 * @generated
 */
public interface Movie extends MediaContent {
	/**
	 * Returns the value of the '<em><b>Video</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Video</em>' containment reference.
	 * @see #setVideo(Video)
	 * @see Netflix.NetflixPackage#getMovie_Video()
	 * @model containment="true" required="true"
	 * @generated
	 */
	Video getVideo();

	/**
	 * Sets the value of the '{@link Netflix.Movie#getVideo <em>Video</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Video</em>' containment reference.
	 * @see #getVideo()
	 * @generated
	 */
	void setVideo(Video value);

} // Movie
