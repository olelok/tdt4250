/**
 */
package Netflix;

import java.io.File;
import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Video</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link Netflix.Video#getLength <em>Length</em>}</li>
 *   <li>{@link Netflix.Video#getMediaFile <em>Media File</em>}</li>
 *   <li>{@link Netflix.Video#getSubtitles <em>Subtitles</em>}</li>
 * </ul>
 *
 * @see Netflix.NetflixPackage#getVideo()
 * @model
 * @generated
 */
public interface Video extends Playable {
	/**
	 * Returns the value of the '<em><b>Length</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Length</em>' attribute.
	 * @see #setLength(int)
	 * @see Netflix.NetflixPackage#getVideo_Length()
	 * @model
	 * @generated
	 */
	int getLength();

	/**
	 * Sets the value of the '{@link Netflix.Video#getLength <em>Length</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Length</em>' attribute.
	 * @see #getLength()
	 * @generated
	 */
	void setLength(int value);

	/**
	 * Returns the value of the '<em><b>Media File</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Media File</em>' attribute.
	 * @see #setMediaFile(File)
	 * @see Netflix.NetflixPackage#getVideo_MediaFile()
	 * @model dataType="Netflix.MediaFile" required="true"
	 * @generated
	 */
	File getMediaFile();

	/**
	 * Sets the value of the '{@link Netflix.Video#getMediaFile <em>Media File</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Media File</em>' attribute.
	 * @see #getMediaFile()
	 * @generated
	 */
	void setMediaFile(File value);

	/**
	 * Returns the value of the '<em><b>Subtitles</b></em>' containment reference list.
	 * The list contents are of type {@link Netflix.Subtitle}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Subtitles</em>' containment reference list.
	 * @see Netflix.NetflixPackage#getVideo_Subtitles()
	 * @model containment="true"
	 * @generated
	 */
	EList<Subtitle> getSubtitles();

} // Video
