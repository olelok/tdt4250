/**
 */
package Netflix;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Media Content</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link Netflix.MediaContent#getName <em>Name</em>}</li>
 *   <li>{@link Netflix.MediaContent#getAgeLimit <em>Age Limit</em>}</li>
 *   <li>{@link Netflix.MediaContent#getGenres <em>Genres</em>}</li>
 *   <li>{@link Netflix.MediaContent#getActors <em>Actors</em>}</li>
 * </ul>
 *
 * @see Netflix.NetflixPackage#getMediaContent()
 * @model abstract="true"
 * @generated
 */
public interface MediaContent extends EObject {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see Netflix.NetflixPackage#getMediaContent_Name()
	 * @model
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link Netflix.MediaContent#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Age Limit</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Age Limit</em>' attribute.
	 * @see #setAgeLimit(int)
	 * @see Netflix.NetflixPackage#getMediaContent_AgeLimit()
	 * @model
	 * @generated
	 */
	int getAgeLimit();

	/**
	 * Sets the value of the '{@link Netflix.MediaContent#getAgeLimit <em>Age Limit</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Age Limit</em>' attribute.
	 * @see #getAgeLimit()
	 * @generated
	 */
	void setAgeLimit(int value);

	/**
	 * Returns the value of the '<em><b>Genres</b></em>' reference list.
	 * The list contents are of type {@link Netflix.Genre}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Genres</em>' reference list.
	 * @see Netflix.NetflixPackage#getMediaContent_Genres()
	 * @model required="true"
	 * @generated
	 */
	EList<Genre> getGenres();

	/**
	 * Returns the value of the '<em><b>Actors</b></em>' reference list.
	 * The list contents are of type {@link Netflix.Actor}.
	 * It is bidirectional and its opposite is '{@link Netflix.Actor#getContributions <em>Contributions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Actors</em>' reference list.
	 * @see Netflix.NetflixPackage#getMediaContent_Actors()
	 * @see Netflix.Actor#getContributions
	 * @model opposite="contributions"
	 * @generated
	 */
	EList<Actor> getActors();

} // MediaContent
