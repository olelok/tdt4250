/**
 */
package Netflix;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see Netflix.NetflixFactory
 * @model kind="package"
 * @generated
 */
public interface NetflixPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "Netflix";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "https://www.netflix.com/";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "webpage";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	NetflixPackage eINSTANCE = Netflix.impl.NetflixPackageImpl.init();

	/**
	 * The meta object id for the '{@link Netflix.impl.ProfileImpl <em>Profile</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see Netflix.impl.ProfileImpl
	 * @see Netflix.impl.NetflixPackageImpl#getProfile()
	 * @generated
	 */
	int PROFILE = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROFILE__NAME = 0;

	/**
	 * The feature id for the '<em><b>Favorites</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROFILE__FAVORITES = 1;

	/**
	 * The number of structural features of the '<em>Profile</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROFILE_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Profile</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROFILE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link Netflix.impl.MediaContentImpl <em>Media Content</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see Netflix.impl.MediaContentImpl
	 * @see Netflix.impl.NetflixPackageImpl#getMediaContent()
	 * @generated
	 */
	int MEDIA_CONTENT = 1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEDIA_CONTENT__NAME = 0;

	/**
	 * The feature id for the '<em><b>Age Limit</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEDIA_CONTENT__AGE_LIMIT = 1;

	/**
	 * The feature id for the '<em><b>Genres</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEDIA_CONTENT__GENRES = 2;

	/**
	 * The feature id for the '<em><b>Actors</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEDIA_CONTENT__ACTORS = 3;

	/**
	 * The number of structural features of the '<em>Media Content</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEDIA_CONTENT_FEATURE_COUNT = 4;

	/**
	 * The number of operations of the '<em>Media Content</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEDIA_CONTENT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link Netflix.impl.GenreImpl <em>Genre</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see Netflix.impl.GenreImpl
	 * @see Netflix.impl.NetflixPackageImpl#getGenre()
	 * @generated
	 */
	int GENRE = 2;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GENRE__NAME = 0;

	/**
	 * The number of structural features of the '<em>Genre</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GENRE_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Genre</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GENRE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link Netflix.impl.ActorImpl <em>Actor</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see Netflix.impl.ActorImpl
	 * @see Netflix.impl.NetflixPackageImpl#getActor()
	 * @generated
	 */
	int ACTOR = 3;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTOR__NAME = 0;

	/**
	 * The feature id for the '<em><b>Age</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTOR__AGE = 1;

	/**
	 * The feature id for the '<em><b>Contributions</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTOR__CONTRIBUTIONS = 2;

	/**
	 * The feature id for the '<em><b>Firstname</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTOR__FIRSTNAME = 3;

	/**
	 * The feature id for the '<em><b>Last Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTOR__LAST_NAME = 4;

	/**
	 * The number of structural features of the '<em>Actor</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTOR_FEATURE_COUNT = 5;

	/**
	 * The number of operations of the '<em>Actor</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTOR_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link Netflix.impl.MovieImpl <em>Movie</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see Netflix.impl.MovieImpl
	 * @see Netflix.impl.NetflixPackageImpl#getMovie()
	 * @generated
	 */
	int MOVIE = 4;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOVIE__NAME = MEDIA_CONTENT__NAME;

	/**
	 * The feature id for the '<em><b>Age Limit</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOVIE__AGE_LIMIT = MEDIA_CONTENT__AGE_LIMIT;

	/**
	 * The feature id for the '<em><b>Genres</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOVIE__GENRES = MEDIA_CONTENT__GENRES;

	/**
	 * The feature id for the '<em><b>Actors</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOVIE__ACTORS = MEDIA_CONTENT__ACTORS;

	/**
	 * The feature id for the '<em><b>Video</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOVIE__VIDEO = MEDIA_CONTENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Movie</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOVIE_FEATURE_COUNT = MEDIA_CONTENT_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Movie</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOVIE_OPERATION_COUNT = MEDIA_CONTENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link Netflix.impl.SeriesImpl <em>Series</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see Netflix.impl.SeriesImpl
	 * @see Netflix.impl.NetflixPackageImpl#getSeries()
	 * @generated
	 */
	int SERIES = 5;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERIES__NAME = MEDIA_CONTENT__NAME;

	/**
	 * The feature id for the '<em><b>Age Limit</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERIES__AGE_LIMIT = MEDIA_CONTENT__AGE_LIMIT;

	/**
	 * The feature id for the '<em><b>Genres</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERIES__GENRES = MEDIA_CONTENT__GENRES;

	/**
	 * The feature id for the '<em><b>Actors</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERIES__ACTORS = MEDIA_CONTENT__ACTORS;

	/**
	 * The feature id for the '<em><b>Seasons</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERIES__SEASONS = MEDIA_CONTENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Series</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERIES_FEATURE_COUNT = MEDIA_CONTENT_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Series</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERIES_OPERATION_COUNT = MEDIA_CONTENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link Netflix.impl.EpisodeImpl <em>Episode</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see Netflix.impl.EpisodeImpl
	 * @see Netflix.impl.NetflixPackageImpl#getEpisode()
	 * @generated
	 */
	int EPISODE = 6;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EPISODE__NAME = 0;

	/**
	 * The feature id for the '<em><b>Video</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EPISODE__VIDEO = 1;

	/**
	 * The number of structural features of the '<em>Episode</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EPISODE_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Episode</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EPISODE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link Netflix.impl.SeasonImpl <em>Season</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see Netflix.impl.SeasonImpl
	 * @see Netflix.impl.NetflixPackageImpl#getSeason()
	 * @generated
	 */
	int SEASON = 7;

	/**
	 * The feature id for the '<em><b>Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEASON__NUMBER = 0;

	/**
	 * The feature id for the '<em><b>Episodes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEASON__EPISODES = 1;

	/**
	 * The number of structural features of the '<em>Season</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEASON_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Season</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEASON_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link Netflix.Playable <em>Playable</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see Netflix.Playable
	 * @see Netflix.impl.NetflixPackageImpl#getPlayable()
	 * @generated
	 */
	int PLAYABLE = 8;

	/**
	 * The number of structural features of the '<em>Playable</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLAYABLE_FEATURE_COUNT = 0;

	/**
	 * The operation id for the '<em>Play</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLAYABLE___PLAY = 0;

	/**
	 * The operation id for the '<em>Pause</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLAYABLE___PAUSE = 1;

	/**
	 * The number of operations of the '<em>Playable</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLAYABLE_OPERATION_COUNT = 2;

	/**
	 * The meta object id for the '{@link Netflix.impl.VideoImpl <em>Video</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see Netflix.impl.VideoImpl
	 * @see Netflix.impl.NetflixPackageImpl#getVideo()
	 * @generated
	 */
	int VIDEO = 9;

	/**
	 * The feature id for the '<em><b>Length</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VIDEO__LENGTH = PLAYABLE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Media File</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VIDEO__MEDIA_FILE = PLAYABLE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Subtitles</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VIDEO__SUBTITLES = PLAYABLE_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Video</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VIDEO_FEATURE_COUNT = PLAYABLE_FEATURE_COUNT + 3;

	/**
	 * The operation id for the '<em>Play</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VIDEO___PLAY = PLAYABLE___PLAY;

	/**
	 * The operation id for the '<em>Pause</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VIDEO___PAUSE = PLAYABLE___PAUSE;

	/**
	 * The number of operations of the '<em>Video</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VIDEO_OPERATION_COUNT = PLAYABLE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link Netflix.impl.SubtitleImpl <em>Subtitle</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see Netflix.impl.SubtitleImpl
	 * @see Netflix.impl.NetflixPackageImpl#getSubtitle()
	 * @generated
	 */
	int SUBTITLE = 10;

	/**
	 * The feature id for the '<em><b>Language</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBTITLE__LANGUAGE = 0;

	/**
	 * The feature id for the '<em><b>Display Texts</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBTITLE__DISPLAY_TEXTS = 1;

	/**
	 * The number of structural features of the '<em>Subtitle</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBTITLE_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Subtitle</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBTITLE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link Netflix.impl.DisplayTextImpl <em>Display Text</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see Netflix.impl.DisplayTextImpl
	 * @see Netflix.impl.NetflixPackageImpl#getDisplayText()
	 * @generated
	 */
	int DISPLAY_TEXT = 11;

	/**
	 * The feature id for the '<em><b>Text</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DISPLAY_TEXT__TEXT = 0;

	/**
	 * The feature id for the '<em><b>Start Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DISPLAY_TEXT__START_TIME = 1;

	/**
	 * The feature id for the '<em><b>End Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DISPLAY_TEXT__END_TIME = 2;

	/**
	 * The number of structural features of the '<em>Display Text</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DISPLAY_TEXT_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>Display Text</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DISPLAY_TEXT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link Netflix.impl.UserImpl <em>User</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see Netflix.impl.UserImpl
	 * @see Netflix.impl.NetflixPackageImpl#getUser()
	 * @generated
	 */
	int USER = 12;

	/**
	 * The feature id for the '<em><b>Profiles</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER__PROFILES = 0;

	/**
	 * The feature id for the '<em><b>Email</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER__EMAIL = 1;

	/**
	 * The feature id for the '<em><b>Password</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER__PASSWORD = 2;

	/**
	 * The number of structural features of the '<em>User</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>User</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER_OPERATION_COUNT = 0;


	/**
	 * The meta object id for the '<em>Media File</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see java.io.File
	 * @see Netflix.impl.NetflixPackageImpl#getMediaFile()
	 * @generated
	 */
	int MEDIA_FILE = 13;


	/**
	 * The meta object id for the '<em>Birth Date</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see java.util.Date
	 * @see Netflix.impl.NetflixPackageImpl#getBirthDate()
	 * @generated
	 */
	int BIRTH_DATE = 14;


	/**
	 * Returns the meta object for class '{@link Netflix.Profile <em>Profile</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Profile</em>'.
	 * @see Netflix.Profile
	 * @generated
	 */
	EClass getProfile();

	/**
	 * Returns the meta object for the attribute '{@link Netflix.Profile#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see Netflix.Profile#getName()
	 * @see #getProfile()
	 * @generated
	 */
	EAttribute getProfile_Name();

	/**
	 * Returns the meta object for the containment reference list '{@link Netflix.Profile#getFavorites <em>Favorites</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Favorites</em>'.
	 * @see Netflix.Profile#getFavorites()
	 * @see #getProfile()
	 * @generated
	 */
	EReference getProfile_Favorites();

	/**
	 * Returns the meta object for class '{@link Netflix.MediaContent <em>Media Content</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Media Content</em>'.
	 * @see Netflix.MediaContent
	 * @generated
	 */
	EClass getMediaContent();

	/**
	 * Returns the meta object for the attribute '{@link Netflix.MediaContent#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see Netflix.MediaContent#getName()
	 * @see #getMediaContent()
	 * @generated
	 */
	EAttribute getMediaContent_Name();

	/**
	 * Returns the meta object for the attribute '{@link Netflix.MediaContent#getAgeLimit <em>Age Limit</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Age Limit</em>'.
	 * @see Netflix.MediaContent#getAgeLimit()
	 * @see #getMediaContent()
	 * @generated
	 */
	EAttribute getMediaContent_AgeLimit();

	/**
	 * Returns the meta object for the reference list '{@link Netflix.MediaContent#getGenres <em>Genres</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Genres</em>'.
	 * @see Netflix.MediaContent#getGenres()
	 * @see #getMediaContent()
	 * @generated
	 */
	EReference getMediaContent_Genres();

	/**
	 * Returns the meta object for the reference list '{@link Netflix.MediaContent#getActors <em>Actors</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Actors</em>'.
	 * @see Netflix.MediaContent#getActors()
	 * @see #getMediaContent()
	 * @generated
	 */
	EReference getMediaContent_Actors();

	/**
	 * Returns the meta object for class '{@link Netflix.Genre <em>Genre</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Genre</em>'.
	 * @see Netflix.Genre
	 * @generated
	 */
	EClass getGenre();

	/**
	 * Returns the meta object for the attribute '{@link Netflix.Genre#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see Netflix.Genre#getName()
	 * @see #getGenre()
	 * @generated
	 */
	EAttribute getGenre_Name();

	/**
	 * Returns the meta object for class '{@link Netflix.Actor <em>Actor</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Actor</em>'.
	 * @see Netflix.Actor
	 * @generated
	 */
	EClass getActor();

	/**
	 * Returns the meta object for the attribute '{@link Netflix.Actor#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see Netflix.Actor#getName()
	 * @see #getActor()
	 * @generated
	 */
	EAttribute getActor_Name();

	/**
	 * Returns the meta object for the attribute '{@link Netflix.Actor#getAge <em>Age</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Age</em>'.
	 * @see Netflix.Actor#getAge()
	 * @see #getActor()
	 * @generated
	 */
	EAttribute getActor_Age();

	/**
	 * Returns the meta object for the reference '{@link Netflix.Actor#getContributions <em>Contributions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Contributions</em>'.
	 * @see Netflix.Actor#getContributions()
	 * @see #getActor()
	 * @generated
	 */
	EReference getActor_Contributions();

	/**
	 * Returns the meta object for the attribute '{@link Netflix.Actor#getFirstname <em>Firstname</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Firstname</em>'.
	 * @see Netflix.Actor#getFirstname()
	 * @see #getActor()
	 * @generated
	 */
	EAttribute getActor_Firstname();

	/**
	 * Returns the meta object for the attribute '{@link Netflix.Actor#getLastName <em>Last Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Last Name</em>'.
	 * @see Netflix.Actor#getLastName()
	 * @see #getActor()
	 * @generated
	 */
	EAttribute getActor_LastName();

	/**
	 * Returns the meta object for class '{@link Netflix.Movie <em>Movie</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Movie</em>'.
	 * @see Netflix.Movie
	 * @generated
	 */
	EClass getMovie();

	/**
	 * Returns the meta object for the containment reference '{@link Netflix.Movie#getVideo <em>Video</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Video</em>'.
	 * @see Netflix.Movie#getVideo()
	 * @see #getMovie()
	 * @generated
	 */
	EReference getMovie_Video();

	/**
	 * Returns the meta object for class '{@link Netflix.Series <em>Series</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Series</em>'.
	 * @see Netflix.Series
	 * @generated
	 */
	EClass getSeries();

	/**
	 * Returns the meta object for the containment reference list '{@link Netflix.Series#getSeasons <em>Seasons</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Seasons</em>'.
	 * @see Netflix.Series#getSeasons()
	 * @see #getSeries()
	 * @generated
	 */
	EReference getSeries_Seasons();

	/**
	 * Returns the meta object for class '{@link Netflix.Episode <em>Episode</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Episode</em>'.
	 * @see Netflix.Episode
	 * @generated
	 */
	EClass getEpisode();

	/**
	 * Returns the meta object for the attribute '{@link Netflix.Episode#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see Netflix.Episode#getName()
	 * @see #getEpisode()
	 * @generated
	 */
	EAttribute getEpisode_Name();

	/**
	 * Returns the meta object for the containment reference '{@link Netflix.Episode#getVideo <em>Video</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Video</em>'.
	 * @see Netflix.Episode#getVideo()
	 * @see #getEpisode()
	 * @generated
	 */
	EReference getEpisode_Video();

	/**
	 * Returns the meta object for class '{@link Netflix.Season <em>Season</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Season</em>'.
	 * @see Netflix.Season
	 * @generated
	 */
	EClass getSeason();

	/**
	 * Returns the meta object for the attribute '{@link Netflix.Season#getNumber <em>Number</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Number</em>'.
	 * @see Netflix.Season#getNumber()
	 * @see #getSeason()
	 * @generated
	 */
	EAttribute getSeason_Number();

	/**
	 * Returns the meta object for the containment reference list '{@link Netflix.Season#getEpisodes <em>Episodes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Episodes</em>'.
	 * @see Netflix.Season#getEpisodes()
	 * @see #getSeason()
	 * @generated
	 */
	EReference getSeason_Episodes();

	/**
	 * Returns the meta object for class '{@link Netflix.Playable <em>Playable</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Playable</em>'.
	 * @see Netflix.Playable
	 * @generated
	 */
	EClass getPlayable();

	/**
	 * Returns the meta object for the '{@link Netflix.Playable#play() <em>Play</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Play</em>' operation.
	 * @see Netflix.Playable#play()
	 * @generated
	 */
	EOperation getPlayable__Play();

	/**
	 * Returns the meta object for the '{@link Netflix.Playable#pause() <em>Pause</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Pause</em>' operation.
	 * @see Netflix.Playable#pause()
	 * @generated
	 */
	EOperation getPlayable__Pause();

	/**
	 * Returns the meta object for class '{@link Netflix.Video <em>Video</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Video</em>'.
	 * @see Netflix.Video
	 * @generated
	 */
	EClass getVideo();

	/**
	 * Returns the meta object for the attribute '{@link Netflix.Video#getLength <em>Length</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Length</em>'.
	 * @see Netflix.Video#getLength()
	 * @see #getVideo()
	 * @generated
	 */
	EAttribute getVideo_Length();

	/**
	 * Returns the meta object for the attribute '{@link Netflix.Video#getMediaFile <em>Media File</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Media File</em>'.
	 * @see Netflix.Video#getMediaFile()
	 * @see #getVideo()
	 * @generated
	 */
	EAttribute getVideo_MediaFile();

	/**
	 * Returns the meta object for the containment reference list '{@link Netflix.Video#getSubtitles <em>Subtitles</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Subtitles</em>'.
	 * @see Netflix.Video#getSubtitles()
	 * @see #getVideo()
	 * @generated
	 */
	EReference getVideo_Subtitles();

	/**
	 * Returns the meta object for class '{@link Netflix.Subtitle <em>Subtitle</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Subtitle</em>'.
	 * @see Netflix.Subtitle
	 * @generated
	 */
	EClass getSubtitle();

	/**
	 * Returns the meta object for the attribute '{@link Netflix.Subtitle#getLanguage <em>Language</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Language</em>'.
	 * @see Netflix.Subtitle#getLanguage()
	 * @see #getSubtitle()
	 * @generated
	 */
	EAttribute getSubtitle_Language();

	/**
	 * Returns the meta object for the containment reference list '{@link Netflix.Subtitle#getDisplayTexts <em>Display Texts</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Display Texts</em>'.
	 * @see Netflix.Subtitle#getDisplayTexts()
	 * @see #getSubtitle()
	 * @generated
	 */
	EReference getSubtitle_DisplayTexts();

	/**
	 * Returns the meta object for class '{@link Netflix.DisplayText <em>Display Text</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Display Text</em>'.
	 * @see Netflix.DisplayText
	 * @generated
	 */
	EClass getDisplayText();

	/**
	 * Returns the meta object for the attribute '{@link Netflix.DisplayText#getText <em>Text</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Text</em>'.
	 * @see Netflix.DisplayText#getText()
	 * @see #getDisplayText()
	 * @generated
	 */
	EAttribute getDisplayText_Text();

	/**
	 * Returns the meta object for the attribute '{@link Netflix.DisplayText#getStartTime <em>Start Time</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Start Time</em>'.
	 * @see Netflix.DisplayText#getStartTime()
	 * @see #getDisplayText()
	 * @generated
	 */
	EAttribute getDisplayText_StartTime();

	/**
	 * Returns the meta object for the attribute '{@link Netflix.DisplayText#getEndTime <em>End Time</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>End Time</em>'.
	 * @see Netflix.DisplayText#getEndTime()
	 * @see #getDisplayText()
	 * @generated
	 */
	EAttribute getDisplayText_EndTime();

	/**
	 * Returns the meta object for class '{@link Netflix.User <em>User</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>User</em>'.
	 * @see Netflix.User
	 * @generated
	 */
	EClass getUser();

	/**
	 * Returns the meta object for the containment reference list '{@link Netflix.User#getProfiles <em>Profiles</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Profiles</em>'.
	 * @see Netflix.User#getProfiles()
	 * @see #getUser()
	 * @generated
	 */
	EReference getUser_Profiles();

	/**
	 * Returns the meta object for the attribute '{@link Netflix.User#getEmail <em>Email</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Email</em>'.
	 * @see Netflix.User#getEmail()
	 * @see #getUser()
	 * @generated
	 */
	EAttribute getUser_Email();

	/**
	 * Returns the meta object for the attribute '{@link Netflix.User#getPassword <em>Password</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Password</em>'.
	 * @see Netflix.User#getPassword()
	 * @see #getUser()
	 * @generated
	 */
	EAttribute getUser_Password();

	/**
	 * Returns the meta object for data type '{@link java.io.File <em>Media File</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Media File</em>'.
	 * @see java.io.File
	 * @model instanceClass="java.io.File"
	 * @generated
	 */
	EDataType getMediaFile();

	/**
	 * Returns the meta object for data type '{@link java.util.Date <em>Birth Date</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Birth Date</em>'.
	 * @see java.util.Date
	 * @model instanceClass="java.util.Date"
	 * @generated
	 */
	EDataType getBirthDate();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	NetflixFactory getNetflixFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link Netflix.impl.ProfileImpl <em>Profile</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see Netflix.impl.ProfileImpl
		 * @see Netflix.impl.NetflixPackageImpl#getProfile()
		 * @generated
		 */
		EClass PROFILE = eINSTANCE.getProfile();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PROFILE__NAME = eINSTANCE.getProfile_Name();

		/**
		 * The meta object literal for the '<em><b>Favorites</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PROFILE__FAVORITES = eINSTANCE.getProfile_Favorites();

		/**
		 * The meta object literal for the '{@link Netflix.impl.MediaContentImpl <em>Media Content</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see Netflix.impl.MediaContentImpl
		 * @see Netflix.impl.NetflixPackageImpl#getMediaContent()
		 * @generated
		 */
		EClass MEDIA_CONTENT = eINSTANCE.getMediaContent();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MEDIA_CONTENT__NAME = eINSTANCE.getMediaContent_Name();

		/**
		 * The meta object literal for the '<em><b>Age Limit</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MEDIA_CONTENT__AGE_LIMIT = eINSTANCE.getMediaContent_AgeLimit();

		/**
		 * The meta object literal for the '<em><b>Genres</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MEDIA_CONTENT__GENRES = eINSTANCE.getMediaContent_Genres();

		/**
		 * The meta object literal for the '<em><b>Actors</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MEDIA_CONTENT__ACTORS = eINSTANCE.getMediaContent_Actors();

		/**
		 * The meta object literal for the '{@link Netflix.impl.GenreImpl <em>Genre</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see Netflix.impl.GenreImpl
		 * @see Netflix.impl.NetflixPackageImpl#getGenre()
		 * @generated
		 */
		EClass GENRE = eINSTANCE.getGenre();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute GENRE__NAME = eINSTANCE.getGenre_Name();

		/**
		 * The meta object literal for the '{@link Netflix.impl.ActorImpl <em>Actor</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see Netflix.impl.ActorImpl
		 * @see Netflix.impl.NetflixPackageImpl#getActor()
		 * @generated
		 */
		EClass ACTOR = eINSTANCE.getActor();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ACTOR__NAME = eINSTANCE.getActor_Name();

		/**
		 * The meta object literal for the '<em><b>Age</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ACTOR__AGE = eINSTANCE.getActor_Age();

		/**
		 * The meta object literal for the '<em><b>Contributions</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACTOR__CONTRIBUTIONS = eINSTANCE.getActor_Contributions();

		/**
		 * The meta object literal for the '<em><b>Firstname</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ACTOR__FIRSTNAME = eINSTANCE.getActor_Firstname();

		/**
		 * The meta object literal for the '<em><b>Last Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ACTOR__LAST_NAME = eINSTANCE.getActor_LastName();

		/**
		 * The meta object literal for the '{@link Netflix.impl.MovieImpl <em>Movie</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see Netflix.impl.MovieImpl
		 * @see Netflix.impl.NetflixPackageImpl#getMovie()
		 * @generated
		 */
		EClass MOVIE = eINSTANCE.getMovie();

		/**
		 * The meta object literal for the '<em><b>Video</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MOVIE__VIDEO = eINSTANCE.getMovie_Video();

		/**
		 * The meta object literal for the '{@link Netflix.impl.SeriesImpl <em>Series</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see Netflix.impl.SeriesImpl
		 * @see Netflix.impl.NetflixPackageImpl#getSeries()
		 * @generated
		 */
		EClass SERIES = eINSTANCE.getSeries();

		/**
		 * The meta object literal for the '<em><b>Seasons</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SERIES__SEASONS = eINSTANCE.getSeries_Seasons();

		/**
		 * The meta object literal for the '{@link Netflix.impl.EpisodeImpl <em>Episode</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see Netflix.impl.EpisodeImpl
		 * @see Netflix.impl.NetflixPackageImpl#getEpisode()
		 * @generated
		 */
		EClass EPISODE = eINSTANCE.getEpisode();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EPISODE__NAME = eINSTANCE.getEpisode_Name();

		/**
		 * The meta object literal for the '<em><b>Video</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EPISODE__VIDEO = eINSTANCE.getEpisode_Video();

		/**
		 * The meta object literal for the '{@link Netflix.impl.SeasonImpl <em>Season</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see Netflix.impl.SeasonImpl
		 * @see Netflix.impl.NetflixPackageImpl#getSeason()
		 * @generated
		 */
		EClass SEASON = eINSTANCE.getSeason();

		/**
		 * The meta object literal for the '<em><b>Number</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SEASON__NUMBER = eINSTANCE.getSeason_Number();

		/**
		 * The meta object literal for the '<em><b>Episodes</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SEASON__EPISODES = eINSTANCE.getSeason_Episodes();

		/**
		 * The meta object literal for the '{@link Netflix.Playable <em>Playable</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see Netflix.Playable
		 * @see Netflix.impl.NetflixPackageImpl#getPlayable()
		 * @generated
		 */
		EClass PLAYABLE = eINSTANCE.getPlayable();

		/**
		 * The meta object literal for the '<em><b>Play</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation PLAYABLE___PLAY = eINSTANCE.getPlayable__Play();

		/**
		 * The meta object literal for the '<em><b>Pause</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation PLAYABLE___PAUSE = eINSTANCE.getPlayable__Pause();

		/**
		 * The meta object literal for the '{@link Netflix.impl.VideoImpl <em>Video</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see Netflix.impl.VideoImpl
		 * @see Netflix.impl.NetflixPackageImpl#getVideo()
		 * @generated
		 */
		EClass VIDEO = eINSTANCE.getVideo();

		/**
		 * The meta object literal for the '<em><b>Length</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute VIDEO__LENGTH = eINSTANCE.getVideo_Length();

		/**
		 * The meta object literal for the '<em><b>Media File</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute VIDEO__MEDIA_FILE = eINSTANCE.getVideo_MediaFile();

		/**
		 * The meta object literal for the '<em><b>Subtitles</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference VIDEO__SUBTITLES = eINSTANCE.getVideo_Subtitles();

		/**
		 * The meta object literal for the '{@link Netflix.impl.SubtitleImpl <em>Subtitle</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see Netflix.impl.SubtitleImpl
		 * @see Netflix.impl.NetflixPackageImpl#getSubtitle()
		 * @generated
		 */
		EClass SUBTITLE = eINSTANCE.getSubtitle();

		/**
		 * The meta object literal for the '<em><b>Language</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SUBTITLE__LANGUAGE = eINSTANCE.getSubtitle_Language();

		/**
		 * The meta object literal for the '<em><b>Display Texts</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SUBTITLE__DISPLAY_TEXTS = eINSTANCE.getSubtitle_DisplayTexts();

		/**
		 * The meta object literal for the '{@link Netflix.impl.DisplayTextImpl <em>Display Text</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see Netflix.impl.DisplayTextImpl
		 * @see Netflix.impl.NetflixPackageImpl#getDisplayText()
		 * @generated
		 */
		EClass DISPLAY_TEXT = eINSTANCE.getDisplayText();

		/**
		 * The meta object literal for the '<em><b>Text</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DISPLAY_TEXT__TEXT = eINSTANCE.getDisplayText_Text();

		/**
		 * The meta object literal for the '<em><b>Start Time</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DISPLAY_TEXT__START_TIME = eINSTANCE.getDisplayText_StartTime();

		/**
		 * The meta object literal for the '<em><b>End Time</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DISPLAY_TEXT__END_TIME = eINSTANCE.getDisplayText_EndTime();

		/**
		 * The meta object literal for the '{@link Netflix.impl.UserImpl <em>User</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see Netflix.impl.UserImpl
		 * @see Netflix.impl.NetflixPackageImpl#getUser()
		 * @generated
		 */
		EClass USER = eINSTANCE.getUser();

		/**
		 * The meta object literal for the '<em><b>Profiles</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference USER__PROFILES = eINSTANCE.getUser_Profiles();

		/**
		 * The meta object literal for the '<em><b>Email</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute USER__EMAIL = eINSTANCE.getUser_Email();

		/**
		 * The meta object literal for the '<em><b>Password</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute USER__PASSWORD = eINSTANCE.getUser_Password();

		/**
		 * The meta object literal for the '<em>Media File</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see java.io.File
		 * @see Netflix.impl.NetflixPackageImpl#getMediaFile()
		 * @generated
		 */
		EDataType MEDIA_FILE = eINSTANCE.getMediaFile();

		/**
		 * The meta object literal for the '<em>Birth Date</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see java.util.Date
		 * @see Netflix.impl.NetflixPackageImpl#getBirthDate()
		 * @generated
		 */
		EDataType BIRTH_DATE = eINSTANCE.getBirthDate();

	}

} //NetflixPackage
