/**
 */
package Netflix;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Episode</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link Netflix.Episode#getName <em>Name</em>}</li>
 *   <li>{@link Netflix.Episode#getVideo <em>Video</em>}</li>
 * </ul>
 *
 * @see Netflix.NetflixPackage#getEpisode()
 * @model
 * @generated
 */
public interface Episode extends EObject {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see Netflix.NetflixPackage#getEpisode_Name()
	 * @model
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link Netflix.Episode#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Video</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Video</em>' containment reference.
	 * @see #setVideo(Video)
	 * @see Netflix.NetflixPackage#getEpisode_Video()
	 * @model containment="true"
	 * @generated
	 */
	Video getVideo();

	/**
	 * Sets the value of the '{@link Netflix.Episode#getVideo <em>Video</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Video</em>' containment reference.
	 * @see #getVideo()
	 * @generated
	 */
	void setVideo(Video value);

} // Episode
