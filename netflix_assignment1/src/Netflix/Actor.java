/**
 */
package Netflix;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Actor</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link Netflix.Actor#getName <em>Name</em>}</li>
 *   <li>{@link Netflix.Actor#getAge <em>Age</em>}</li>
 *   <li>{@link Netflix.Actor#getContributions <em>Contributions</em>}</li>
 *   <li>{@link Netflix.Actor#getFirstname <em>Firstname</em>}</li>
 *   <li>{@link Netflix.Actor#getLastName <em>Last Name</em>}</li>
 * </ul>
 *
 * @see Netflix.NetflixPackage#getActor()
 * @model annotation="http://www.eclipse.org/emf/2002/Ecore constraints='minAge'"
 *        annotation="http://www.eclipse.org/acceleo/query/1.0 minAge='self.age &gt; 0 '"
 * @generated
 */
public interface Actor extends EObject {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * The default value is <code>""</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see Netflix.NetflixPackage#getActor_Name()
	 * @model default="" required="true" derived="true"
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link Netflix.Actor#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Age</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Age</em>' attribute.
	 * @see #setAge(int)
	 * @see Netflix.NetflixPackage#getActor_Age()
	 * @model
	 * @generated
	 */
	int getAge();

	/**
	 * Sets the value of the '{@link Netflix.Actor#getAge <em>Age</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Age</em>' attribute.
	 * @see #getAge()
	 * @generated
	 */
	void setAge(int value);

	/**
	 * Returns the value of the '<em><b>Contributions</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link Netflix.MediaContent#getActors <em>Actors</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Contributions</em>' reference.
	 * @see #setContributions(MediaContent)
	 * @see Netflix.NetflixPackage#getActor_Contributions()
	 * @see Netflix.MediaContent#getActors
	 * @model opposite="actors" required="true"
	 * @generated
	 */
	MediaContent getContributions();

	/**
	 * Sets the value of the '{@link Netflix.Actor#getContributions <em>Contributions</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Contributions</em>' reference.
	 * @see #getContributions()
	 * @generated
	 */
	void setContributions(MediaContent value);

	/**
	 * Returns the value of the '<em><b>Firstname</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Firstname</em>' attribute.
	 * @see #setFirstname(String)
	 * @see Netflix.NetflixPackage#getActor_Firstname()
	 * @model
	 * @generated
	 */
	String getFirstname();

	/**
	 * Sets the value of the '{@link Netflix.Actor#getFirstname <em>Firstname</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Firstname</em>' attribute.
	 * @see #getFirstname()
	 * @generated
	 */
	void setFirstname(String value);

	/**
	 * Returns the value of the '<em><b>Last Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Last Name</em>' attribute.
	 * @see #setLastName(String)
	 * @see Netflix.NetflixPackage#getActor_LastName()
	 * @model
	 * @generated
	 */
	String getLastName();

	/**
	 * Sets the value of the '{@link Netflix.Actor#getLastName <em>Last Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Last Name</em>' attribute.
	 * @see #getLastName()
	 * @generated
	 */
	void setLastName(String value);

} // Actor
